import React, { useContext } from "react"
import { Link } from "react-router-dom";
import { UserContext } from "../context/UserContext";
import { Button, Navbar, Nav, NavDropdown, Form, FormControl, Container }  from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';

const Header =() =>{
  const [user, setUser] = useContext(UserContext)
  const handleLogout = () =>{
    setUser(null)
    localStorage.removeItem("user")
  }

  return(    
    <Container>
      <Navbar bg="light" expand="lg">
        <Navbar.Brand href="#home">React-Bootstrap</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="mr-auto">
            <Nav.Link><Link to="/">Home</Link></Nav.Link>
            <Nav.Link><Link to="/about">About </Link></Nav.Link>
            {user && <Nav.Link><Link to="/movies">Movie List Editor </Link></Nav.Link>}
            {user === null && <Nav.Link><Link to="/login">Login </Link></Nav.Link>}
            {user && <Nav.Link style={{cursor: "pointer"}} onClick={handleLogout}>Logout</Nav.Link>}
            
            {/* <Nav.Link href="#link">Link</Nav.Link>
            <NavDropdown title="Dropdown" id="basic-nav-dropdown">
              <NavDropdown.Item href="#action/3.1">Action</NavDropdown.Item>
              <NavDropdown.Item href="#action/3.2">Another action</NavDropdown.Item>
              <NavDropdown.Item href="#action/3.3">Something</NavDropdown.Item>
              <NavDropdown.Divider />
              <NavDropdown.Item href="#action/3.4">Separated link</NavDropdown.Item>
            </NavDropdown> */}
          </Nav>
          <Form inline>
            <FormControl type="text" placeholder="Search" className="mr-sm-2" />
            <Button variant="outline-success">Search</Button>
          </Form>
        </Navbar.Collapse>
      </Navbar>
    </Container>
    // <header>
    //   <img id="logo" src="/img/logo.png" width="200px" />
    //   <nav>
    //     <ul>
    //       <li><Link to="/">Home</Link></li>
    //       <li><Link to="/about">About </Link> </li>
    //       { user && <li><Link to="/movies">Movie List Editor </Link></li> }
    //       { user === null && <li><Link to="/login">Login </Link></li> }
    //       { user && <li><a style={{cursor: "pointer"}} onClick={handleLogout}>Logout </a></li> }
    //     </ul>
    //   </nav>
    // </header>
  )
}

export default Header